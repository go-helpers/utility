package components

import "fmt"

const (
	sqlDriverMySQL    = "mysql"
	sqlDriverPostgres = "postgres"
	sqlDsnPattern     = "host=%s port=%d user=%s password=%s dbname=%s sslmode=%s"
)

func sqlDsn(host, user, password, dbName, sslMode string, port int) string {
	return fmt.Sprintf(sqlDsnPattern, host, port, user, password, dbName, sslMode)
}
