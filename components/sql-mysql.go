package components

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// MustMySQL panics or returns a *sql.DB
func MustMySQL(host, user, password, dbName, sslMode string, port int, customDriver ...string) *sql.DB {
	db, err := MySQL(host, user, password, dbName, sslMode, port, customDriver...)
	if err != nil {
		panic(err)
	}
	return db
}

// MySQL  returns a *sql.DB , or error
func MySQL(host, user, password, dbName, sslMode string, port int, customDriver ...string) (*sql.DB, error) {
	driver := sqlDriverMySQL
	if len(customDriver) != 0 {
		driver = customDriver[0]
	}

	dsn := sqlDsn(host, user, password, dbName, sslMode, port)

	return sql.Open(driver, dsn)
}

// MySQLX returns a *sqlx.DB pointer, or error
func MySQLX(host, user, password, dbName, sslMode string, port int, customDriver ...string) (*sqlx.DB, error) {
	db, err := MySQL(host, user, password, dbName, sslMode, port, customDriver...)
	if err != nil {
		return nil, err
	}
	return sqlx.NewDb(db, sqlDriverMySQL), nil
}
func MustMySQLX(host, user, password, dbName, sslMode string, port int, customDriver ...string) *sqlx.DB {
	dbx, err := MySQLX(host, user, password, dbName, sslMode, port, customDriver...)
	if err != nil {
		panic(err)
	}
	return dbx
}
