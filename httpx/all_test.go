package httpx_test

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-helpers/utility/httpx"
)

var testServer = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, client")
}))

func TestGet(t *testing.T) {
	tt := &Topt{
		asserter: assert.New(t),
	}
	err := httpx.Get(tt)
	tt.asserter.NoError(err)
}
func TestGetWithOptionsAndCustomValues(t *testing.T) {
	tt := &Topt{
		asserter: assert.New(t),
	}
	setters := []httpx.OptionSetter{
		httpx.WithRetry(1),
		httpx.WithBackoff(time.Second),
		httpx.WithBeforeRequest(func (txn httpx.Transaction, req *http.Request) error {
			topt := txn.(*Topt)
			topt.foo = "Bar"
			return nil
		}),
		httpx.WithParseResponse(func (txn httpx.Transaction, req *http.Response) error {
			topt := txn.(*Topt)
			topt.asserter.Equal(topt.foo, "Bar")
			return nil
		}),
	}

	err := httpx.Get(tt, setters...)
	tt.asserter.NoError(err)
}

func TestPut(t *testing.T) {
	tt := &Topt{
		asserter: assert.New(t),
	}
	err := httpx.Put(tt)
	tt.asserter.NoError(err)
}
func TestPost(t *testing.T) {
	tt := &Topt{
		asserter: assert.New(t),
	}
	err := httpx.PostWithRetry(tt, 2, time.Millisecond*2)
	tt.asserter.NoError(err)
}

type Topt struct {
	asserter *assert.Assertions
	foo string
}

func (*Topt) Endpoint() string {
	return testServer.URL

}
func (*Topt) Timeout() *time.Duration {
	return nil
}
func (*Topt) Headers() map[string]string {
	return nil
}
func (*Topt) QueryParams() map[string]interface{} {
	return nil
}
func (*Topt) Body() io.Reader {
	return nil
}

// Actions
func (*Topt) ParseHeaders(http.Header) error {
	return nil
}
func (t *Topt) ParseBody(i io.ReadCloser) error {
	bod, err := ioutil.ReadAll(i)
	if err != nil {
		return err
	}
	t.asserter.Equal(string(bod), "Hello, client")
	return nil
}
func (*Topt) ParseStatusCode(i int) {
}
func (*Topt) Log(x int, y string, z interface{}) {
	fmt.Println("log", y, z)
}
