package httpx

import (
	"net/http"
	"time"
)

type CallOption struct {
  BeforeRequest		func(t Transaction, req *http.Request) error
	ParseResponse   func(t Transaction, res *http.Response) error
	Retries 				int
	Backoff 				time.Duration
}

type OptionSetter func(*CallOption)

func WithBeforeRequest(handler func(t Transaction, req *http.Request) error) OptionSetter {
	return func(opt *CallOption) {
		opt.BeforeRequest = handler
	}
}

func WithParseResponse(handler func(t Transaction, res *http.Response) error) OptionSetter {
	return func(opt *CallOption) {
		opt.ParseResponse = handler
	}
}

func WithRetry(retries int) OptionSetter {
	return func(opt *CallOption) {
		opt.Retries = retries
	}
}

func WithBackoff(dur time.Duration) OptionSetter {
	return func(opt *CallOption) {
		opt.Backoff = dur
	}
}

func NewCallOption(setters ...OptionSetter) *CallOption {
	const (
		defaultRetries = 0
		defaultBackoff = 0
	)

	defaultBeforeRequestHandler := func(t Transaction, req *http.Request) error {
		return nil
	}
	defaultParseResponseHandler := func(t Transaction, res *http.Response) error {
		return nil
	}

	opt := &CallOption{
		Retries: defaultRetries,
		Backoff: defaultBackoff,
		BeforeRequest: defaultBeforeRequestHandler,
		ParseResponse: defaultParseResponseHandler,
	}

	for _, setter := range setters {
		setter(opt)
	}

	return opt
}
