package httpx

import (
	"net/http"
	"time"
)

// Post deoes a http.Post request on transaction
func Post(t Transaction, setters ...OptionSetter) (err error) {
	return call(http.MethodPost, t, setters...)

}

// PostWithRetry deoes a http.Post request on transaction, with exp backoff on retries
func PostWithRetry(t Transaction, retries int, backoff time.Duration) (err error) {
	return callRetry(http.MethodPost, t, retries, backoff)
}
