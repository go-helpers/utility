package httpx

import (
	"net/http"
	"time"
)

// Put deoes a http.Post request on transaction
func Put(t Transaction, setters ...OptionSetter) (err error) {
	return call(http.MethodPut, t, setters...)
}

// PutWithRetry deoes a http.Post request on transaction, with exp backoff on retries
func PutWithRetry(t Transaction, retries int, backoff time.Duration) (err error) {
	return callRetry(http.MethodPut, t, retries, backoff)
}
