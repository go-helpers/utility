package httpx

import (
	"fmt"
	"net/http"
	"time"
)

func newClient(timeout *time.Duration) (client *http.Client) {
	client = &http.Client{}
	if timeout != nil {
		client.Timeout = *timeout
	}
	return
}

func setQueryParams(req *http.Request, params map[string]interface{}) {
	query := req.URL.Query()
	for k, v := range params {
		s := fmt.Sprintf("%v", v)
		query.Set(k, s)
	}
	req.URL.RawQuery = query.Encode()
}

func setHeaders(req *http.Request, headers map[string]string) {
	for k, v := range headers {
		req.Header.Set(k, v)
	}
}
