package httpx

import (
	"io"
	"net/http"
	"time"
)

// Transaction interface used by package
type Transaction interface {
	// Getters
	Endpoint() string
	Timeout() *time.Duration
	Headers() map[string]string
	QueryParams() map[string]interface{}
	Body() io.Reader

	// Actions
	ParseHeaders(http.Header) error
	ParseBody(io.ReadCloser) error
	ParseStatusCode(int)
	Log(int, string, interface{}) //for logging
}

